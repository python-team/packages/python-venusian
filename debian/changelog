python-venusian (3.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Enable autopkgtest-pkg-pybuild.

 -- Colin Watson <cjwatson@debian.org>  Wed, 04 Dec 2024 09:47:03 +0000

python-venusian (3.1.0-2) unstable; urgency=medium

  * Team upload.
  * Accommodate FrameLocalsProxy introduction in Python 3.13 (closes:
    #1082279).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Wed, 06 Nov 2024 02:58:08 +0000

python-venusian (3.1.0-1) unstable; urgency=medium

  * New upstream version 3.1.0

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 14 Nov 2023 11:06:32 +0900

python-venusian (3.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 16:02:35 -0400

python-venusian (3.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ TANIGUCHI Takaki ]
  * d/control: Update Homepage URL.
  * Add CI config
  * New upstream version 3.0.0
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * debian/rules: Don't run coverage

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 05 Apr 2020 17:45:34 +0900

python-venusian (1.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support (Closes: #938247).

 -- Andrey Rahmatullin <wrar@debian.org>  Fri, 27 Sep 2019 00:57:43 +0500

python-venusian (1.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ TANIGUCHI Takaki ]
  * New upstream version 1.2.0
  * Bump Stanrads-Version to 4.3.0
  * debian/compat: 12

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 27 Feb 2019 17:05:53 +0900

python-venusian (1.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ TANIGUCHI Takaki ]
  * New upstream version 1.1.0
  * debian/control: Add python-pytest-conv to B-D.
  * debian/rules: Run pytest.
  * debian/compat: Bump to 10.
  * debian/docs: Add README.rst
  * Bump Standards-Version to 4.0.0

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 04 Jul 2017 14:46:39 +0900

python-venusian (1.0a8-3) unstable; urgency=medium

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

  [ TANIGUCHI Takaki ]
  * Bump Standards-Version 3.9.6.
  * debian/watch: Switch to pypy.debian.net.

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 25 Nov 2015 23:53:41 +0900

python-venusian (1.0a8-2) unstable; urgency=medium

  * debian/control:
    - Bumped Standards-Version to 3.9.5 with no other changes necessary.
    - Added X-Python-Version and X-Python3-Version headers.
    - Added python3-venusian binary package.
    - Added Python 3 Build-Depends.
    - wrap-and-sort
  * debian/rules:
    - Convert to --buildsystem=pybuild
    - Override default pybuild auto tests.

 -- Barry Warsaw <barry@debian.org>  Thu, 19 Jun 2014 11:00:47 -0400

python-venusian (1.0a8-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ TANIGUCHI Takaki ]
  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 08 May 2013 18:41:58 +0900

python-venusian (1.0a6-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 09 May 2012 11:29:07 +0900

python-venusian (1.0a3-2) unstable; urgency=low

  * Oops, update debian/copyright URI, again.

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 26 Feb 2012 19:06:56 +0900

python-venusian (1.0a3-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.3.
    + Update debian/copyright URI.

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 26 Feb 2012 19:01:07 +0900

python-venusian (1.0a2-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 06 Oct 2011 21:23:38 +0900

python-venusian (1.0a1-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 02 Oct 2011 17:35:16 +0900

python-venusian (0.9-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 20 Jun 2011 10:45:34 +0900

python-venusian (0.7-4) unstable; urgency=low

  * Team upload.
  * Add python-nose to Build-Depends (Closes: #625126)
  * Standards-Version bumped to 3.9.2 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 22 May 2011 00:17:16 +0200

python-venusian (0.7-3) unstable; urgency=low

  * Team upload.
  * Rebuild to add Python 2.7 support

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 08 May 2011 16:45:36 +0200

python-venusian (0.7-2) unstable; urgency=low

  * debian/copyright: Add copyright infomation.  (Closes: #612791)

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 05 Apr 2011 23:12:08 +0900

python-venusian (0.7-1) unstable; urgency=low

  * New upstream release
  * debian/rules: switch to dh_python2

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 21 Mar 2011 11:38:02 +0900

python-venusian (0.6-1) unstable; urgency=low

  * New upstream release
  * debian/clean: Added.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 11 Feb 2011 12:45:41 +0900

python-venusian (0.4-1) unstable; urgency=low

  * Initial release (Closes: #604998)

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 26 Nov 2010 10:23:39 +0900
